## About
This project is a submission to a coding challenge from `MVST GmbH`.
It is a simple search for repositories given a certain Github username. It sends a request to Github Graphql api v4, and it is tested using `jest`.

### Built with
- React
- Redux
- Graphql

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

## Demo
Find a demo video [here](https://www.loom.com/share/33a6a0e1f122421f86ab2cc142f9deb3)

## Installation
1. Clone the repo
2. Install npm packages
    ```sh
    npm install
    ```
3. Get a Github access token from https://github.com/settings/tokens
4. Rename the file `.env.default` to `.env`
5. Paste the access token in `.env`
    ```sh
   REACT_APP_GH_TOKEN=YOUR_ACCESS_TOKEN
    ```

## Usage
To run the project in dev mode, run `npm start`.    
To build, run `npm build`.
 
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!
