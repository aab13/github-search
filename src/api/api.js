import {gql, GraphQLClient} from 'graphql-request'

/* Fetches all public repositories owned by a user
 * The maximum number of repositories that could be fetched is 100 (per Github limitation)
 * I decided not to include it because few users have over 100 repositories
 * and this feature could be added in the next iteration
 */
export function getRepositoriesByUser(username, lastCursor = null) {
    const query = gql`
        query getReposName {user(login: "${username}") {
            repositories(isFork: false, last:${lastCursor} first: 100, orderBy: {field: UPDATED_AT, direction: DESC}) {
                pageInfo {
                    endCursor
                    hasNextPage
                    hasPreviousPage
                    startCursor
                }
                nodes {
                    name
                    description
                    updatedAt
                    forkCount
                    isFork
                    stargazerCount
                    licenseInfo {
                        name
                    }
                    primaryLanguage {
                        name
                    }
                }
            }
        }
    }`;

    const endpoint = 'https://api.github.com/graphql';

    const graphQLClient = new GraphQLClient(endpoint, {
        headers: {
            authorization: `Bearer ${process.env.REACT_APP_GH_TOKEN}`,
        },
    })

    return graphQLClient.request(query);
}
