import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import Container from './containers/container.js';

import './App.css';

function App() {
  return (
      <React.Fragment>
          <Router>
              <React.Fragment>
                  <Route path="/" component={Container} />
              </React.Fragment>
          </Router>
      </React.Fragment>
  );
}

export default App;
