import {connect} from 'react-redux'

import RepositoriesByUser from '../components/repositoriesByUser.js';
import getReposByUserHandler from './getReposByUserHandler';

const mapStateToProps = (state) => {
    return {
        repos: state.repos || [],
        errors: state.errors,
        loading: state.loading,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getReposByUser: (username) => getReposByUserHandler(dispatch, username)
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RepositoriesByUser);