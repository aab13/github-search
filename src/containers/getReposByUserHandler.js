import {actions} from "../store/slice";
import {getRepositoriesByUser} from "../api/api";

/* Function to be used by the container
 * I put it in a single file so that it could be tested
 */
const getReposByUserHandler = (dispatch, username) => {
    // Declare the start the process of getting repositories from Github
    dispatch(actions.getRepos());
    // fetch repositories
    return getRepositoriesByUser(username)
        .then((response) => {
            // Case: User is not found or if there was an error from Github
            if (!response.user) {
                const payload = [response.message || 'something went wrong.. the user is null'];
                return dispatch(actions.getReposFailure(payload));
            }
            // Case: User is found and fetching user's repositories is successful
            const repos = response.user.repositories.nodes;
            return dispatch(actions.getReposSuccess(repos));
        })
        .catch(err => {
            // Case: The request throws an error
            let payload = null;
            if (err.response && err.response.errors) {
                payload = err.response.errors.map(e => e.message);
            } else if (err.message) {
                payload = [err.message];
            } else {
                payload = ['Something unexpected happened'];
                console.error(err); // to be substituted by a logger for production
            }
            return dispatch(actions.getReposFailure(payload));
        })
};

export default getReposByUserHandler;