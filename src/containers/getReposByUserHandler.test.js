import React from "react";
import {afterEach, beforeEach, describe, expect, it} from "@jest/globals";
import configureStore from 'redux-mock-store';
import { QueryMock } from 'graphql-query-test-mock';
import {cleanup} from "@testing-library/react";

import {reducer} from '../store/slice';
import getReposByUserHandler from "./getReposByUserHandler";

const queryMock = new QueryMock();
const mockStore = configureStore({reducer});

const initialState = {
    repos: [],
    errors: null,
    loading: false,
};

const endpoint = 'https://api.github.com/graphql';

beforeEach(() => {
    queryMock.setup(endpoint);
});
afterEach(cleanup);

describe('getReposByUserHandler behavior', () => {
        it('Does a graphql request', async () => {
            const expectedPayload = [1, 2, 3];

            queryMock.mockQuery({
                name: 'getReposName',
                data: {
                    user: {
                        repositories: {
                            nodes: expectedPayload
                        }
                    },
                    message: null,
                    errors: null
                }
            });

            const store = mockStore(initialState);

            await getReposByUserHandler(store.dispatch, 'username');

            const actions = store.getActions();
            const types = actions.map(a => a.type);
            const payloads = actions.map(a => a.payload);

            expect(types).toEqual(['repos/getRepos', 'repos/getReposSuccess']);

            expect(payloads[0]).toBeUndefined();
            expect(payloads[1]).toBeTruthy();
            expect(payloads[1]).toEqual(expectedPayload);

            store.clearActions();
        });

        it('Handles a failed graphql request', async () => {
            const errorMsg = ['This is an error message'];

            queryMock.mockQuery({
                name: 'getReposName',
                data: {
                    user: null,
                    message: errorMsg[0],
                    errors: errorMsg
                }
            });

            const store = mockStore(initialState);

            await getReposByUserHandler(store.dispatch, 'username');

            const actions = store.getActions();
            const types = actions.map(a => a.type);
            const payloads = actions.map(a => a.payload);

            expect(types).toEqual(['repos/getRepos', 'repos/getReposFailure']);

            expect(payloads[0]).toBeUndefined();
            expect(payloads[1]).toBeTruthy();
            expect(payloads[1]).toEqual(errorMsg);


            store.clearActions();
        });

        it('Handles a faulty graphql request', async () => {
            queryMock.mockQuery({
                name: 'getReposNa',
                data: {
                    user: {
                        repositories: {
                            nodes: [1, 2, 3]
                        }
                    },
                    message: null,
                    errors: null
                }
            });

            const store = mockStore(initialState);

            await getReposByUserHandler(store.dispatch, 'username');

            const actions = store.getActions();
            const types = actions.map(a => a.type);
            const payloads = actions.map(a => a.payload);

            expect(types).toEqual(['repos/getRepos', 'repos/getReposFailure']);

            expect(payloads[0]).toBeUndefined();
            expect(payloads[1]).toBeTruthy();

            store.clearActions();
        });
    }
);

