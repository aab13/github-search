import React from 'react';
import {TextInput, Button} from '@primer/components';
import {SearchIcon} from "@primer/octicons-react";

import RepositoryItem from "./repositoryItem";

import './repositoriesByUser.css';

/* Displays the result of the search query
 * A user can search search by username, then can search in another filter by repository name
 * It handles a non-existant username and empty list
 * Displays a message when the user doesn't exist or if the request resulted in an error
 */
class RepositoriesByUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            queryExists: false,
            repoSearchEntry: ''
        }

        const searchParams = new URLSearchParams(this.props.location.search);
        const query = searchParams.get('username');
        if (query) {
            this.props.getReposByUser(query);
            this.state.username = query;
            this.state.queryExists = true;
        }


        this.handleRepoSearchEntryChange = this.handleRepoSearchEntryChange.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handleFindUserRepos = this.handleFindUserRepos.bind(this);
        this.updateURL = this.updateURL.bind(this);
    }

    // Takes care of the back button case
    componentDidUpdate(prevProps) {
        const searchParams = new URLSearchParams(this.props.location.search);
        const prevSearchParams = new URLSearchParams(prevProps.location.search);

        const query = searchParams.get('username') || '';
        const prevQuery = prevSearchParams.get('username') || '';
        if (query !== prevQuery) {
            this.setState({username: query, queryExists: true});
            this.props.getReposByUser(query);
        }
        window.onpopstate = e => {
            this.setState({username: query, queryExists: true});
            if (query && query !== '' && query !== prevQuery) {
                this.props.getReposByUser(query);
            }
        }
    }

    handleRepoSearchEntryChange(event) {
        this.setState({repoSearchEntry: event.target.value});
    }

    handleUsernameChange(event) {
        this.setState({username: event.target.value});
    }

    handleFindUserRepos(event) {
        event.preventDefault();
        this.updateURL();
        this.props.getReposByUser(this.state.username);
    }

     // Updates the query in the url
    updateURL() {
        const searchParams = new URLSearchParams();
        searchParams.set("username", this.state.username);
        const url = searchParams.toString();
        this.props.history.push(`?${url}`);
    };

    render() {
        return(
            <div className={`${this.state.queryExists ? "container" : "center"}`}>
                <form className={"username-form"} onSubmit={this.handleFindUserRepos} data-testid="search-form">
                    <TextInput ml={4} icon={SearchIcon} placeholder="Find repositories by user" type="text" value={this.state.username} onChange={this.handleUsernameChange}/>
                    <Button type="submit">Find</Button>
                </form>
                {
                    this.props.errors && !this.props.loading && this.state.queryExists
                        ?
                            <div className={"error-msg"}>
                                <h3>Looks like something went wrong...</h3>
                                <p>{this.props.errors}</p>
                            </div>
                        :
                            <div className={"result-container"}>
                                {
                                    // Display search bar if the repo list is not an empty list
                                    !!this.props.repos.length
                                        && <TextInput ml={4} className={"search-bar"} icon={SearchIcon} placeholder="Find repository" type="text" value={this.state.repoSearchEntry} onChange={this.handleRepoSearchEntryChange}/>
                                }
                                <div className={"repositories-list"}>
                                    {
                                        // Repository name filter is applied here
                                        (this.props.repos || []).map((repo, i) => repo.name.includes(this.state.repoSearchEntry)
                                            && <RepositoryItem key={i} repo={repo} username={this.state.username}/>)
                                    }
                                </div>
                            </div>
                }
            </div>
        );
    }
}

export default RepositoriesByUser;