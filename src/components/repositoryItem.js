import {Link} from '@primer/components';
import moment from "moment";
import {LawIcon, RepoForkedIcon, StarIcon} from "@primer/octicons-react";

import './repositoryItem.css';

// Displays a single row from the list of repositories
function RepositoryItem ({repo, username}) {
    const url = `https://github.com/${username}/${repo.name}`;

    return (
        <div className="box">
            <Link className="link" mb={1} href={url} target="_blank">
                <h3>{repo.name}</h3>
            </Link>
            <p className="description">{repo.description}</p>
            <div className="info">
                {repo.primaryLanguage && <span className="language">{repo.primaryLanguage.name}</span>}
                <span className="stars"> <StarIcon size={16} />  {repo.stargazerCount}</span>
                <span className="language"> <RepoForkedIcon size={16} /> {repo.forkCount}</span>
                {repo.licenseInfo && <span className="language"><LawIcon size={16}/> {repo.licenseInfo.name}</span>}
                <span className="last-update">Last updated {moment(new Date(repo.updatedAt).toISOString()).fromNow()}</span>
            </div>
        </div>
    );
}

export default RepositoryItem;