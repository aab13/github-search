import {describe, expect, it} from "@jest/globals";
import {reducer, actions} from './slice';

const initialState = {
    repos: [],
    errors: null,
    loading: false,
};

describe('reducer tests', () => {
    it('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('actions: getRepos', () => {
        expect(reducer(initialState, actions.getRepos)).toEqual({
            ...initialState,
            loading: true,
        });
    });

    it('actions: getReposFailure', () => {
        expect(reducer(initialState, actions.getReposFailure).errors).not.toEqual(null);
        expect(reducer(initialState, actions.getReposFailure).loading).toEqual(false);
    });

    it('actions: getReposSuccess', () => {
        expect(reducer(initialState, {
            type: actions.getReposSuccess,
            payload: [{}, {}, {}],
        })).toEqual({
            ...initialState,
            repos: [{}, {}, {}],
        });
    });
});