import {createSlice} from "@reduxjs/toolkit";
// Redux generates actions and reducer from a slice
const INITIAL_STATE = {
    repos: [],
    errors: null,
    loading: false,
}

const reposSlice = createSlice({
    name: 'repos',
    initialState: INITIAL_STATE,
    reducers: {
        getRepos(state) {
            return {
                ...state,
                repos: [],
                loading: true,
                errors: null
            }
        },
        getReposSuccess(state, data) {
            return {
                ...state,
                repos: data.payload,
                loading: false,
                errors: null
            }
        },
        getReposFailure(state, data) {
            return {
                ...state,
                repos: [],
                loading: false,
                errors: data.payload
            }
        }
    },
});

/* I chose to export the actions as an object instead of exporting each action individually for faster development time
 * and better readability
 * Another way to implement a store could be by manually creating action types and mapping them to each action
 * before adding it to the reducer
 */
export const {actions, reducer}  = reposSlice;
