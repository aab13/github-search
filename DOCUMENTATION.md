# Documentation

## API `./src/api`
### `getRepositoriesByUser`
Fetches all public repositories owned by a user.

Arguments:
- `username`: String. Required. Github username of the user of whom to fetch the repositories.
- `lastCursor`: String. Optional. If Github API returns more than 100 request, redo the request and specify the cursor of the last element fetched. Default value is `null`.


## Components `./src/components`
### RepositoriesByUser
Displays the result of the search query.

Usage:
```html
<RepositoriesByUser/>
```

### RepositoryItem
Displays a single row from the list of repositories.

Usage:
```html
<RepositoryItem repo={REPOSITORY} username={USERNAME}/>
```
`REPOSITORY` object must have the same schema as the one fetched from Github.

## Containers `./src/containers`
### Container
Wrapper around `RepositoriesByUser`. Usage:
```html
<Container/>
```

### `getReposByUserHandler`
Function to be called by Redux when the `getRepos` action is dispatched.

Arguments:
- `dispatch`: (callback). Required. Must be the dispatch function from the store.
- `username`: String. Required. Github username of the user of whom to fetch the repositories.